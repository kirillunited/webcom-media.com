var Bundle = function () {};

Bundle.prototype.buffer = [];

Bundle.prototype.executes = function () {
    this.buffer.forEach(function (class_instance) {
        return new class_instance().init();
    }.bind(this));
};

window.addEventListener('load', function () {
    return new Bundle().executes();
});

var _ = function () {
    this.container = $('.b-examples-advert-new__list.js-carousel') || 0;
    this.prevState = false;
};

_.prototype.init = function () {
    $(window).resize((this.resize).bind(this));
    this.resize();
};

_.prototype.getState = function () {

    var width = $('body').innerWidth();
    switch (true) {
        case width <= 767:
            return 'mobile';
            break;
        case (width >= 768) && (width <= 1023):
            return 'tablet';
            break;
        case width >= 1024:
            return 'desktop';
            break;
    }
}

_.prototype.resize = function () {
    var state = this.getState();
    if (state != this.prevState && this.container.length) {
        switch (state) {
            case 'desktop':
                this.container.trigger('destroy.owl.carousel');
                this.container.removeClass('owl-carousel');
                break;
            case 'tablet':
                this.container.trigger('destroy.owl.carousel');
                this.container.removeClass('owl-carousel');
                break;
            case 'mobile':
                this.container.trigger('destroy.owl.carousel');
                this.container.addClass('owl-carousel');
                this.container.owlCarousel({
                    dots: true,
                    nav: false,
                    loop: true,
                    items: 1,
                    mouseDrag: true,
                    touchDrag: true,
                    autoHeight: true,
                });
                break;
        }
    }
    this.prevState = state;
};

Bundle.prototype.buffer.push(_);

var _ = function() {
    this.questions = $('.b-faq-v2 .b-element__question');
    this.elements = $('.b-faq-v2__elements');
};

_.prototype.init = function () {
    this.questions.click(this.callback.bind(this));
};

_.prototype.callback = function (event) {
    for (var i = 0; i < this.questions.length; i++ ) {
        if (event.currentTarget.classList.contains('b-element__question--no-arrow')) {
            break;
        }
        else if ( this.questions[i] != event.currentTarget 
            && this.questions[i].classList.contains('js-active')) {
                this.questions[i].classList.remove('js-active');
                break;
        }
    }
    if (!event.currentTarget.classList.contains('b-element__question--no-arrow')) {
        $(event.currentTarget).toggleClass('js-active');
    }
};

// Scroll to top of screen for active element
// $('.b-faq-v2 .b-element__question').on('click', function(e){
//     elements = $('.b-faq-v2__elements');
//     currentElement = $(this).parent();
//     currentElementIndex = currentElement.index();
//     offsetTop = elements.offset().top + currentElementIndex * ($(this).outerHeight() + 8) - 80;
//     $("html, body").stop().animate({scrollTop:offsetTop}, 300);
// })

Bundle.prototype.buffer.push(_);
