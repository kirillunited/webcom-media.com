/***************************************
 * Developer: MacFiss (Makar Gerasimov) *
 * Company: Webcom Media                *
 ***************************************/

var bModal = function() {
    if (typeof jQuery === 'undefined') {
        return console.log('bModal: Sorry, jQuery is required');
    }

    this.config = new Object();

    this.comparison = {
        'title': 'b-form__title',
        'sub_title': 'b-form__desc'
        // 'fields': 'b-form__fields'
    };

    this.template = {
        input: '<input class="b-form__input" name="{name}" placeholder="{placeholder}">',

        textarea: '<textarea class="b-form__text" name="{name}" rows="3" placeholder="{placeholder}"></textarea>'
    };

    /**
     * Primary initialization
     */
    this.init = function() {
        this.body = $('body');
        this.modal = this.body.find('.b-new-modal');
        this.state = {
            validate: new bModalValidate(),
            values: new Object(),
            mark: false
        };

        this.body.on('click', '[data-b-new-modal]', this.initModal.bind(this));
        this.modal.on('click', '.b-form__btn', this.submit.bind(this));
        this.modal.on('click', '[data-b-new-modal-close]', this.close.bind(this));
        this.modal.on('input change', Object.keys(this.template).join(), this.readyChange.bind(this));
        this.closeEvent();
    };

    /**
     * Field validation during a live change
     * @param  {object} e event
     */
    this.readyChange = function(e) {
        if (typeof this.config[this.state.mark].fields[e.target.name] === 'undefined') {
            return false;
        }

        var element = this.config[this.state.mark].fields[e.target.name];

        if (!element.rules) {
            return false;
        }

        var field = this.modal.find(e.target);

        if (typeof field === 'undefined') {
            return console.log('bModal: the field is not found in DOM');
        }

        field
            .removeClass('b-form__input--invalid')
            .parent().find('.b-modal__help')
            .empty()
            .addClass('b-modal__help--hide');

        for (var rule in element.rules) {
            if (typeof this.state.validate[rule] === 'undefined') {
                console.log('bModal: the "' + rule + '" filter is missing');
                continue;
            }

            if (!this.state.validate.testing(e.target.name, rule, field.val(), element.rules[rule])) {
                var message = this.state.validate.get(e.target.name);

                if (message) {
                    field.parent().find('.b-modal__help')
                        .empty()
                        .text(message)
                        .removeClass('b-modal__help--hide');
                }

                field.addClass('b-form__input--invalid');
            }
        }
    };

    /**
     * Field validation and initialization callback's
     */
    this.submit = function() {
        var config = this.config[this.state.mark];
        if (typeof config.fields === 'undefined') {
            return false;
        }

        if ('beforeValidate' in config) {
            config.beforeValidate(this);
        }

        this.modal.find(Object.keys(this.template).join()).each(function(i, e) {
            this.readyChange({
                target: e
            });
        }.bind(this));

        if (this.state.validate.isError()) {
            if ('validateError' in config) {
                config.validateError(this);
            }
            return false;
        }

        for (var name in config.fields) {
            this.state.values[name] = this.modal.find('[name=' + name + ']').val();
        }

        if ('afterValidate' in config) {
            config.afterValidate(this);
        }

        return false;
    };

    /**
     * Create field element and copy to container
     * @param {string} container class name
     * @param {string} mark      it's marker form
     */
    // this.addFields = function(container) {
    //     if (typeof this.config[this.state.mark].fields === 'undefined') {
    //         return false;
    //     }

    //     var fields = Object.keys(this.config[this.state.mark].fields).reverse();

    //     for (var i = 0; i < fields.length; i++) {

    //         var name = fields[i];
    //         var element = this.config[this.state.mark].fields[name];

    //         if (!(element.type in this.template)) {
    //             console.log('bModal: Sorry, not found the template for the field "' + name + '"');
    //             continue;
    //         }

    //         var star_required = element.rules.required ? '<i>*</i>' : '';

    //         var template = this.template[element.type]
    //             .replace(/\{name\}/g, name)
    //             .replace(/\{label\}/g, element.label + ' ' + star_required)
    //             .replace(/\{placeholder\}/g, element.placeholder ? element.placeholder : '');

    //         var line = element.line ? '<div class="b-modal__line"></div>' : '';

    //         this.modal.find('.' + container).prepend(function() {
    //             return '<div class="b-modal__item">' +
    //                 template +
    //                 '<span class="b-modal__help b-modal__help--hide"></span>' +
    //                 '</div>' +
    //                 line;
    //         });
    //     }
    // };

    /**
     * The render html modal
     * @param  {string} mark it's marker form
     */
    this.generate = function() {
        if (typeof this.comparison === 'undefined') {
            return console.log('bModal: Sorry, is not comparative data');
        }

        for (var name in this.comparison) {
            switch (name) {
                case 'fields':
                    this.addFields(this.comparison[name]);
                    break;
                default:
                    this.modal.find('.' + this.comparison[name]).text(
                        this.get(this.state.mark)[name]
                    );
            }
        }

        this.open();
    };

    /**
     * Initialization modal popup
     * @param  {object} e event object
     * @return {bool}   blocked link transition
     */
    this.initModal = function(e) {
        if (typeof this.modal === 'undefined') {
            return console.log('bModal: DOM not init');
        }

        if( this.state.mark ) {
          return false;
        }

        var mark = e.currentTarget.dataset.bNewModal;

        if (!(mark in this.config)) {
            return console.log('bModal: Popup marker "' + mark + '" not found');
        }

        this.state.mark = mark;

        this.generate(mark);

        return false;
    };

    /**
     * Get all input objects
     * @return {object}
     */
    this.getPostAll = function() {
        return this.state.values;
    };

    /**
     * Is open modal popup
     */
    this.open = function() {
        this.modal.addClass('b-new-modal--active')
            .show();
        this.body.css('overflow', 'hidden');
    };

    /**
     * Is close modal popup
     */
    this.close = function() {
        this.modal.removeClass('b-new-modal--active')
            .hide()
            .removeAttr('style')
        this.body.css('overflow', 'auto');
        this.clear();
    };

    this.closeEvent = function() {
      this.body.mouseup(function (e) {
        return (this.body.find('.b-new-modal').has(e.target).length === 0) ? this.close() : false;
      }.bind(this));

      this.body.keyup(function(e) {
        return (e.keyCode == 27) ? this.close() : false;
      }.bind(this));
    };

    /**
     * Output loading
     * @param  {bool} status
     *         						- true  show
     *         						- false hide
     */
    this.stateLoader = function(status) {
        var information = this.modal.find('.b-form__information');
        var container = this.modal.find('.b-form__loader');
        var elements = this.modal.find('.b-form__fields');

        status ? information.filter(':hidden').removeAttr('style') :
            information.filter(':visible').hide()

        status ? elements.filter(':visible').hide() :
            elements.filter(':hidden').removeAttr('style')

        return status ? container.show() : container.hide();
    };

    /**
     * Conclusion successful treatment
     * @param  {bool} status
     *         						- true  show
     *         						- false hide
     */
    this.stateInformation = function(status) {
        var information = this.modal.find('.b-form__information');
        var container = this.modal.find('.b-form__success');
        var elements = this.modal.find('.b-form__fields');

        status ? information.filter(':hidden').removeAttr('style') :
            information.filter(':visible').hide()

        status ? elements.filter(':visible').hide() :
            elements.filter(':hidden').removeAttr('style')

        return status ? container.css('display','flex') : container.hide();
    };

    this.clear = function() {
        this.stateInformation(false);
        this.state = {
            validate: new bModalValidate(),
            values: new Object(),
            mark: false
        };
        this.modal.find('.' + this.comparison.title).empty();
        this.modal.find('.' + this.comparison.sub_title).empty();
        this.modal.find('.b-form__input--invalid').removeClass('b-form__input--invalid');
        // this.modal.find('[name]').val('');
        // this.modal.find('.' + this.comparison.fields).html(function() {
        //     return $(this).find('.b-form__field').filter(':last');
        // });
    };

    this.clone = function(original) {
        return jQuery.extend(true, {}, original);
    };

    this.set = function(mark, params) {
        if (mark in this.config) {
            return false;
        }

        this.config[mark] = params;
        return true;
    };

    this.get = function(mark) {
        return this.config[mark];
    };

    this.unset = function(mark) {
        return delete this.config[mark];
    };

    window.onload = this.init.bind(this);
};
