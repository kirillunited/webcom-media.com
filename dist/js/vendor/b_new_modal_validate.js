/***************************************
* Developer: MacFiss (Makar Gerasimov) *
* Company: Webcom Media                *
***************************************/

var bModalValidate = function() {
    this.error = new Object();
    this.priority = ['required'];
    this.messages = {
        max_strlen: 'Вы набрали на {count} символов больше допустимого',
        min_strlen: 'Вы должны ввести не менее {min_count} символов',
        required: 'Данное поле является обязательным',
        isName: 'Ваши Ф.И.О. могут содержать только русские и английские буквы',
        isSite: 'Формат ввода адреса: mysite.by',
        isEmail: 'Формат ввода адреса: myemail@service.by',
        isPhone: 'Формат ввода телефона: +375 (00) 000-00-00'
    };

    /**
     * Testing value is max strlen
     * @param  {string} value
     * @param  {int} param
     * @return {object}
     */
    this.max_strlen = function (value, param) {
        var e = new Object;
        return e.status = value.length < param,
            e.replace = new Object,
            e.result || (e.replace.count = Math.floor(value.length - param)),
            e
    };

    /**
     * Testing value is min strlen
     * @param  {string} value
     * @param  {int} param
     * @return {bool}
     */
    this.min_strlen = function (value, param) {
        var e = new Object;
        return e.status = value.length > param,
            e.replace = new Object,
            e.result || (e.replace.min_count = param,
            e.replace.count = value.length),
            e
    };

    /**
     * Testing value is empty
     * @param  {string} value
     * @param  {int} param
     * @return {bool}
     */
    this.required = function(value, param) {
        return {
            status: value.length > 0 || 0 == param
        };
    }

    /**
     * Testing value is site url
     * @param  {string} value
     * @param  {int} param
     * @return {bool}
     */
    this.isSite = function(value, param) {
        return {
            status: !value || value.match(/^(http\:\/\/)?(([а-яёa-z0-9]{1})((\.[а-яёa-z0-9-])|([а-яёa-z0-9-]))*\.([а-яёa-z]{2,6})(\/?)$)/i)
        };
    };

    /**
     * Testing value is valid email
     * @param  {string} value
     * @param  {int} param
     * @return {bool}
     */
    this.isEmail = function(value, param) {
        return {
            status: value.match(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i)
        };
    };

    /**
     * Testing value is valid name
     * @param  {string} value
     * @param  {int} param
     * @return {bool}
     */
    this.isName = function(value, param) {
        return {
            status: value.match(/^([а-яА-ЯёЁa-zA-Z]{3,})/i)
        };
    };

    /**
     * Testing value is valid phone
     * @param  {string} value
     * @param  {int} param
     * @return {bool}
     */
    this.isPhone = function(value, param) {
        // return {
        //     status: value.match(/^(\+?(375|8)[\s]?)(\(?(33|44|25|29|0(25|29|33|44))\)?[\s]?)(\d{3}[-\s]?)(\d{2}[-\s]?){2}$/i)
        // };

        if (!value.indexOf("+375") || !value.indexOf("375") || !value.indexOf("80")) {
            return {
                status: value.match(/^(\+?(375|8)[\s]?)(\(?(17|162|163|1644|165|1655|212|2151|2156|2157|214|232|2334|2351|2342|152|1512|1561|1592|1514|177|1771|1772|1775|1773|1795|174|1717|222|2232|225|2231|2233|2248|2237|2244|2236|2245|2238|2241|2234|2240|2235|2246|2247|2242|2243|2239|33|44|25|29|0(17|162|163|1644|165|1655|212|2151|2156|2157|214|232|2334|2351|2342|152|1512|1561|1592|1514|177|1771|1772|1775|1773|1795|174|1717|222|2232|225|2231|2233|2248|2237|2244|2236|2245|2238|2241|2234|2240|2235|2246|2247|2242|2243|2239|25|29|33|44))\)?[\s]?)(\d{3}[-\s]?)(\d{2}[-\s]?){2}$/i)
            };
        }
        else if (!value.indexOf("+7") || !value.indexOf("7")) {
            return {
                status: value.match(/^(\+?(7)[\s]?)(\(?\d{3}\)?[\s]?)(\d{3}[-\s]?)(\d{2}[-\s]?){2}$/i)
            };
        }
        else {
            return {
                status: value.match(/^(\+\d)/i)
            };
        }
    };

    /**
     * Testing for a specific filter
     * @param  {string} field name field
     * @param  {string} rule  name rule filter
     * @param  {string} value testing value
     * @param  {other}  param to some value
     * @return {bool}
     */
    this.testing = function(field, rule, value, param) {
        var s = this[rule](value, param, field);
        return s.status ? this.remove(field, rule) : this.set(field, rule, s.replace),
        s.status
    };

    /**
     * Message recording an error for a particular field
     * @param {string} field   name field
     * @param {string} rule    name rule filter
     * @param {object} replace property is replace in message
     */
    this.set = function(field, rule, replace) {
        if (!(rule in this.messages))
            return console.log('bModalValidate: filter "' + rule + '" is missing error message');
        field in this.error || this.error[field] instanceof Object || (this.error[field] = new Object),
        this.error[field][rule] = this.messages[rule];
        for (var i in replace)
            this.error[t][rule] = this.error[field][rule].replace(new RegExp('{' + i + '}', 'g'), replace[i]);
        return !0
    };

    /**
     * Magic function
     * Fetching the last element. If it is present in the priority list, we print the first priority
     * @param  {string} field name
     * @return {string}       error message
     */
    this.get = function(field) {
        for (var r = Object.keys(this.error[field]).pop(), e = this.error[field][r], i = 0; i < this.priority.length; i++)
            if (this.priority[i] in this.error[field]) {
                var s = this.priority[i];
                e = this.error[field][s];
                break
            }
        return e
    };

    this.remove = function(field, rule) {
        return field in this.error && (rule in this.error[field] && delete this.error[field][rule],
        void (Object.keys(this.error[field]).length < 1 && delete this.error[field]))
    };

    this.getAll = function() {
        return this.error;
    };

    this.isError = function() {
        return Object.keys(this.error).length > 0;
    };
};
