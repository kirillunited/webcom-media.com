(function($, viewport) {

    var Anonymus = function() {
        var $body = $('body'),
            $window = $(window);

        this.tooltip = function() {
          if ( ! viewport.mobile() ) {
            return false;
          }

          $body.find('[data-tooltip]').hover(function(e) {
            if ( (e.target.offsetLeft + 250) > $window.width() )
              $(this).addClass('g-tooltip-right');
          });
        };

        this.initCRMForm = function() {
            var $_bModal = new bModal();
            var _this = this;

            $.getJSON('/landing-page/target-advert/instagram/js/database.json', function(data) {
                $.each(data, function(label, el) {
                    if (!el.crm.mail_template || !el.crm.label) {
                        return false;
                    }

                    el.afterValidate = _this.ajax_send_crm.bind(_this, el.crm.mail_template, el.crm.label, el.crm.title ? el.crm.title : false);
                    delete el.crm;

                    $_bModal.set(label, el);
                });
            });
        };

        this.ajax_send_crm = function() {
            var _this = arguments[arguments.length - 1];
            var data = _this.getPostAll();
            var config = _this.get(_this.state.mark);

            data.tpl_id = arguments[0];
            data.id = arguments[1];
            data.page_title = ('title' in config && !arguments[2]) ? config.title : arguments[2];
            $.ajax({
                url: '/cms/modules/ajaxSendler/ajaxSendler.php',
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    _this.stateLoader(true);
                },
                error: function() {
                    console.log('ajax_send_crm: Ajax error');
                    _this.stateLoader(false);
                },
                success: function(result) {
                    if (!result.success) {
                        return console.log('ajax_send_crm: Server result is error');
                    }

                    data.type = 'amoTestUnsorted';
                    data.amoCookie = this.getCookie('amoCookie');
                    data.amoCookieLast = this.getCookie('amoCookieLast');

                    $.ajax({
                        url: '/cms/api/amoAPI.php',
                        type: 'POST',
                        data: data
                    });

                    if (typeof dataLayer !== 'undefined') {
                        dataLayer.push({
                            'event': data.id + '_Order'
                        });
                    } else {
                        console.log('ajax_send_crm: dataLayer GTM Google is not init');
                    }

                    _this.stateLoader(false);
                    _this.stateInformation(true);
                }.bind(this)
            });
        };

        this.getCookie = function(name) {
            var value = '; ' + document.cookie;
            var parts = value.split('; ' + name + '=');
            if (parts.length == 2) return parts.pop().split(';').shift();
        };
    };

    $(function() {
        var run = new Anonymus();
        run.initCRMForm();
    });

})(jQuery, device);

function set_demo_data() {
    $('.b-modal__input[name=name]').val('Test31');
    $('.b-modal__input[name=phone]').val('+375 33 000-00-00');
    $('.b-modal__input[name=site]').val('promo-webcom.by');
    $('.b-modal__input[name=email]').val('administrator@promo-webcom.by');
    $('.b-modal__text[name=text]').val('Тестовое сообщение');
}
