(function($) {
    var Anonymus = function() {
        var $body = $('body'),
            $window = $(window);

        $('html').removeClass('no-js');

        this.dogy = function() {
            return new WOW({
                mobile: false,
                offset: 135
            }).init();
        };

        /*
         * Изменение идентификатора формы по клику на кнопку для AMO
         * href - id form
         * data-track-id - метка
         * data-search - если href, не id формы, то необходимо искать форму в этом контейнере (only data - true)
         * Пример кнопки: <a href="#bottomForm" data-track-id="identificator" class="tracking toogleLink bonus_btn">Кнопка identificator</a>
         * */
        this.buttonTracking = function() {
            $body.find('.tracking[data-track-id]').click(function() {
                var _this = $(this);
                var id = _this.data('track-id');
                var form = $body.find(_this.attr('href'));

                if (_this.data('search')) {
                    form = form.find('form');
                }

                form.attr('data-form-id', id);
            });
        };

        /* Кнопка вверх */
        this.slideTop = function() {
            //|| (viewport.tablet() && viewport.portrait())
            if (this.viewport.mobile()) {
                return false;
            }

            var container = $body.find('.page-top');

            $window.scroll(function() {
                if ($(this).scrollTop() > 0) {
                    container.fadeIn();
                } else {
                    container.fadeOut();
                }
            });

            container.click(function() {
                $('body,html').animate({
                    scrollTop: 0
                }, 300);
            });
        };

        /*
         * Переход по ссылке к определенному блоку на странице
         * Пример:
         *  <a href="#myaso">Перейти к мясному блоку</a>
         *   ..большое кол-во блоков..
         *  <div id="myaso">Наша продукция самого наивысшего сорта!</div>
         * */
        this.toogleLink = function() {
            $('a.toogleLink[href*="#"]').click(function() {
                var _target = '#' + $(this).attr('href').split('#').pop();
                $('body,html').animate({
                    scrollTop: $body.find(_target).offset().top - 56
                }, 1500);
                return false;
            });
        };

        /*
         * Расскрытие/закрытие любого блока
         * Пример:
         *  <span class="slideBlockLink">я пример</span>
         *  <div class="slideBlockShow" style="display:none">скрытый текст</div>
         * */
        this.slideBlockLink = function() {
            $body.find('.slideBlockLink').click(function() {
                $(this).parent().parent().find('.slideBlockShow').slideToggle();
            });
        };

        this.pageLoading = function() {
            window.addEventListener('load', function() {
                $body.find('.b-loading-page').animateCss('animated fadeOut', function() {
                    $(this).remove();
                });
            });
        };

        this.previewMedia = function() {
            $body.find('[data-media-preview]').fancybox({
                maxWidth: 800,
                maxHeight: 600,
                fitToView: true,
                width: '70%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                helpers: {
                    overlay: {
                        locked: false
                    },
                    media: {}
                }
            });
        };

        this.previewPdf = function() {
            if ($body.width() > 1199) {
                $body.find('[data-pdf-preview]').fancybox({
                    maxWidth: 1050,
                    width: '100%',
                    type: 'html',
                    autoSize: false,
                    content: '<embed src="{link}#nameddest=self&page=1&view=FitH,0&zoom=80,0,0" type="application/pdf" height="99%" width="100%" />',
                    afterLoad: function() {
                        this.content = this.content.replace(/\{link\}/g, this.href);
                    },
                    beforeClose: function() {
                        $('.fancybox-inner').unwrap();
                    }
                });
            } 
            else {
                $body.find('[data-pdf-preview]').attr('target', '_blank');
            }
        };

        this.toolsThumb = function() {
            $body.find('[data-thumb-init]').fancybox({
                prevEffect: 'fade',
                nextEffect: 'fade',
                scrolling: 'no',
                autoDimensions: true,
                overflow: 'hidden',
                autoScale: true,
                width: 'auto',
                height: 'auto',
                autoSize: true,
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        locked: true
                    },
                    thumbs: {
                        width: 50,
                        height: 50,
                        type: 'inside',
                    }
                },
                beforeShow: function() {
                    if (this.group.length <= 1) {
                        return false;
                    }

                    this.title = (this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
                },
            });
        };
    };



    $(function() {
        var run = new Anonymus();
        run.viewport = device;
        run.pageLoading();
        run.slideBlockLink();
        run.slideTop();
        run.toogleLink();
        run.buttonTracking();
        run.previewMedia();
        run.toolsThumb();
        run.previewPdf();
    });

    $.fn.extend({
        animateCss: function(animationName, callback) {
            callback = callback || function() {};

            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass(animationName).one(animationEnd, function() {
                $(this).removeClass(animationName);
                callback.call(this);
            });
        }
    });
})(jQuery);