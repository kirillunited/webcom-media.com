function CallStack() {
    this.initMap = function (e, a) {
        window.activeMaps || (window.activeMaps = []), window.activeMaps.push(new google.maps.Map(e, a))
    }
}

document.addEventListener("DOMContentLoaded", function (e) {
    $(this);
    var a = {map_styles: ""}, t = "г.Минск, Скрыганова 6а, этаж 4, офис 39", s = "../../images/map/default/map-icon.svg",
        i = new google.maps.LatLng(53.9105873, 27.5168252), l = {
            scrollwheel: !1,
            draggable: !0,
            zoom: 14,
            center: i,
            styles: a.map_styles ? JSON.parse(a.map_styles) : [{
                featureType: "administrative",
                elementType: "labels.text.fill",
                stylers: [{color: "#444444"}]
            }, {featureType: "landscape", elementType: "all", stylers: [{color: "#f2f2f2"}]}, {
                featureType: "poi",
                elementType: "all",
                stylers: [{visibility: "off"}]
            }, {
                featureType: "road",
                elementType: "all",
                stylers: [{saturation: -100}, {lightness: 45}]
            }, {
                featureType: "road.highway",
                elementType: "all",
                stylers: [{visibility: "simplified"}]
            }, {
                featureType: "road.arterial",
                elementType: "labels.icon",
                stylers: [{visibility: "off"}]
            }, {featureType: "transit", elementType: "all", stylers: [{visibility: "off"}]}, {
                featureType: "water",
                elementType: "all",
                stylers: [{color: "#46bcec"}, {visibility: "on"}]
            }]
        }, o = new google.maps.Map(document.querySelector("#gmapContainer"), l);
    if (document.body.clientWidth >= 1200) {
        o.panBy(-350, -50);
    }
    var n = {position: i, map: o};
    n.map_info_title = "Приходите в гости!", n.address = t, n.icon = s;
    var p = new google.maps.Marker(n), r = "";
    r += '<div class="b-mapBox__info">', r += '<div class="b-mapBox__title">Приходите в гости!</div>', r += '<div class="b-mapBox__address">' + t + "</div>", r += "</div>";
    var y = new InfoBox({
        content: r,
        disableAutoPan: !1,
        maxWidth: 150,
        pixelOffset: new google.maps.Size(-230, -211),
        zIndex: null,
        boxStyle: {opacity: 1, width: "auto"},
        closeBoxMargin: "0 0 -15px 0",
        closeBoxURL: "../../images/map/default/close.svg",
        infoBoxClearance: new google.maps.Size(1, 1)
    });
    if (document.body.clientWidth < 768) {
        y = new InfoBox({
            content: r,
            disableAutoPan: !1,
            maxWidth: 150,
            pixelOffset: new google.maps.Size(-150, -200),
            zIndex: null,
            boxStyle: {opacity: 1, width: "auto"},
            closeBoxMargin: "0 0 -15px 0",
            closeBoxURL: "../../images/map/default/close.svg",
            infoBoxClearance: new google.maps.Size(1, 1)
        });
    }
    y.open(o, p), google.maps.event.addListener(p, "click", function () {
        y.open(o, this)
    });
});