$(function () {
    // const COLLAPSE_TITLE = $('.b-collapse__title');

    // $.each(COLLAPSE_TITLE, function (index, elem) {
    //     let btn = $(elem).find('button'),
    //         target = $(elem).next(),
    //         parent = $(elem).parent().siblings();

    //     btn.on('click', function () {
    //         btn.toggleClass('is-active');
    //         target.slideToggle();
    //         parent.find('.b-collapse__block').slideUp().queue(function () {
    //             $(this).siblings('.b-collapse__title').find('button').removeClass('is-active');
    //             $(this).removeClass('is-active');
    //             $(this).dequeue();
    //         });
    //     });
    // });
    $('body').on('click', '[data-toggle]', function (e) {
        e.preventDefault();
        var el = $(this);
        el.toggleClass('is-active');
        el.next('[data-attr="tab"]').toggleClass('is-active');
        el.parent()
            .siblings()
            .find('.is-active').removeClass('is-active');
    });

    $('body').on('submit', '.b-form', function (e) {
        e.preventDefault();
        const FORM = $(this),
            INP = FORM.find('[data-required]'),
            ERROR_MSG = '<div class="is-invalid__msg">Required field</div>',
            INVALID = FORM.find('.is-invalid')

        INVALID.removeClass('is-invalid').find('.is-invalid__msg').remove();

        $(INP).each(function (index, element) {
            if (!$(this).val()) {
                $(this).parent().addClass('is-invalid').append(ERROR_MSG);
            }
        });

        if (FORM.find('.is-invalid').length > 0) return false;

        $.post(
            FORM.attr('action'),
            FORM.serialize()
        );
    });

    $('.js-anchor').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 50
        }, 1000);
        event.preventDefault();
    });

    /** WHO ARE WE slider init */
    aboutSlider();
    $(window).resize(function () {
        aboutSlider();
    });

    /**
     * MOBILE MENU
     */

    /*show/hide mobile menu*/
    $('body').on("click", ".js-mobile-menu", function () {
        var target = $(this).attr('data-target');

        $(this).toggleClass('is-active');
        $('[data-attr="' + target + '"]').toggleClass('is-open');
    });
    $('body').on("click", ".b-navbar__item.js-anchor", function (event) {
        event.preventDefault();
        $('.b-navbar, .js-mobile-menu').removeClass('is-open is-active');
    });
    /*active link on scroll*/
    $(window).scroll(function () {
        var $sections = $('section');
        $sections.each(function (i, el) {
            var top = $(el).offset().top - 60;
            var bottom = top + $(el).height();
            var scroll = $(window).scrollTop();
            var id = $(el).attr('id');
            if (scroll > top && scroll < bottom) {
                $('.b-navbar__item').removeClass('is-active');
                $('a[href="#' + id + '"]').addClass('is-active');
            }
        })
    });

    /**
     * KR variants slider
     */
    $('.b-vars-slider').owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        items: 1,
        navText: ['<span class="arrow-text">Пример 1-ого варианта</span>', '<span class="arrow-text">Пример 2-ого варианта</span>'],
    });
    /* Goes to position*/
    $('.b-options__link').click(function () {
        var id = $(this).attr('data-target'),
            owl = $('.b-vars-slider');

        owl.trigger('to.owl.carousel', [id]);
    });
    /**display prev and next num of slide */
    $('.b-vars-slider').on('changed.owl.carousel', function (event) {
        $(".owl-prev").html('<div class="arrow-text">Пример ' + (event.item.index === 0 ? 1 : event.item.index) + '-ого варианта</div>');
        $(".owl-next").html('<div class="arrow-text">Пример ' + (event.item.index === 0 ? 2 : (event.item.index + 2)) + '-ого варианта</div>');
    });

    //запрет ввода букв
    $('#RK_VALUE, #CALC_AMOUNT').keypress(function (e) {
        if (e.which == 13) {
            $(this).blur();
            return false;
        }
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $('#RK_VALUE, #CALC_AMOUNT').focus(function () {
        $(this).val('');
    });
    /** calculate input range init*/
    $('[data-attr="calculate"]').on('click', function () {
        var defaultValue = $('#RK_VALUE').data('default-value');
        var value = $('#RK_VALUE').val();
        var parseValue = parseInt(value);

        if (parseValue) {
            $('#RK_VALUE').val((parseValue >= defaultValue ? parseValue : defaultValue).toString() + " BYN");
        }

        if ($('.js-range').hasClass('ui-slider')) $('.js-range').slider("destroy");

        toInitCalcRange();
        $('[data-attr="CALC_RESULT"]').slideDown();
        $(this).closest('.b-calc').addClass('is-open');
    });
    $('.b-var-card').each(function (index, element) {
        setTimeout(function () {
            var blink = $('<div class="blink"></div>');

            $(element).prepend(blink);
        }, 100 + 2000 * index)
    });
    /**
    * selfcontrol page
    */
    $('#CALC_AMOUNT').blur(function () {
        var value = $(this).val();
        var parseValue = parseFloat(value).toFixed(2);

        if (parseValue) {
            $(this).val((parseValue >= 100 ? parseValue : 100).toString() + " BYN");
            toCalcResultSumm();
        }
    });
    
    function toCalcResultSumm(params) {
        var a = parseFloat($("#CALC_AMOUNT").val()),
            b = parseFloat($("#CALC_RESULT_FEE").val()),
            c = (a + a / 100 * b).toFixed(2),
            curName_usd = "USD",
            curVal_usd = 0.49,
            curName_rur = "RUR",
            curVal_rur = 30.97;

        $("#CALC_RESULT_SUMM").val(c + " BYN");
        $("#CALC_RESULT_AMOUNT").val((a*curVal_usd).toFixed(2) + " " + curName_usd + " / " + (a*curVal_rur).toFixed(2) + " " + curName_rur);
    }

    toCalcResultSumm();

    $('body').on('change input', '[name="CALC_ITEM"], #CALC_AMOUNT', function () {
        if ($(this).is('[name = "CALC_ITEM"]')) {
            var parseValue = parseInt($(this).val());

            $("#CALC_RESULT_FEE").val(parseValue.toString() + " BYN");
            if ($(this).prop('checked') == true) {
                $(this).closest('.b-self-calc-item').addClass('is-active').siblings().removeClass('is-active');
            }
        }
        toCalcResultSumm();
    });
});

function aboutSlider() {
    if ($('.js-about-slider').length != 0 && typeof $.fn.slick == 'function') {
        if (!$('.js-about-slider').hasClass("slick-initialized") && $(window).width() < 768) {
            $('.js-about-slider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        dots: true,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
                ]
            });
        } else if ($('.js-about-slider').hasClass("slick-initialized") && $(window).width() >= 768) {
            $('.js-about-slider').slick('unslick');
        }
    }
};

function toInitCalcRange() {
    if ($(".js-range") != null || $(".js-range").length > 0) {
        var rkInp = $('#RK_VALUE');
        var rkValue = rkInp.val() ? parseInt(rkInp.val(), 10) : rkInp.data('default-value');
        var minWrap = $('.js-range').siblings('.b-calc-range-min');
        var maxWrap = $('.js-range').siblings('.b-calc-range-max');
        var ID = $('.js-range').data('target');
        var inp = $('#' + ID);
        var start = parseInt(inp.attr('value'), 10),
            min = parseInt(inp.attr('min'), 10),
            max = parseInt(inp.attr('max'), 10),
            step = parseInt(inp.attr('step'), 10);

        $(".js-range").slider({
            range: "min",
            value: start,
            orientation: "horizontal",
            min: min,
            max: max + 1,
            step: step,
            animate: true,
            create: function (event, ui) {
                var value = $(this).slider("value") - 1;
                $('<div class="b-calc-range-tooltip">Месячный бюджет на рекламу:<br> <div class="result"><span data-default-rk-budget="">450</span> BYN</div></div>').appendTo($('.ui-slider-handle'));
                $('[data-default-rk-budget]').text(value);
                toCalculate(rkValue, value);
            },
            slide: function (event, ui) {
                var value = ui.value != 1 ? ui.value - 1 : ui.value;
                $('[data-default-rk-budget]').text(value);
                toCalculate(rkValue, value);
            }
        });
        minWrap.text(min + " BYN");
        maxWrap.text(max + " BYN");
    }
}

function toCalculate(a, b) {
    var c = b < 900 ? 13 : 10,
        d = 14,
        e = a * 2 / (b / 100 * (c + d)),
        f = e.toFixed(2),
        g = Math.round(f * 30),
        h = Math.round(f * b);

    $('[data-total-time]').text(g);
    $('[data-total-price]').text(h);
}