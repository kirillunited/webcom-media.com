/*eslint indent: ["error", 2, { "VariableDeclarator": { "var": 2, "let": 2, "const": 3 } }]*/

window.bonusWidget = (function () {
  var App = function () {
    this.container = this.search('.b-subscribe-gift');
    if(!this.container) return false;

    this.addStyle('/cms/modules/subscribe/bonus/html/css/styles.css');

    this.animation = this.container.classList.contains('b-subscribe-gift--left') ? 'rollIn' : 'rollOut';
   
    this.eventListener('click', this.onBind.bind(this, 'open'), null, this.search('.b-subscribe-gift__btn'));
    this.eventListener('click', this.onBind.bind(this, 'close'), null, this.search('.b-subscribe-modal__close'));
    this.eventListener('click', this.onBind.bind(this, 'submit'), null, this.search('.b-subscribe-modal__submit'));
  };

  App.prototype.addStyle = function (src) {
    var el = document.createElement('link');
    el.rel  = 'stylesheet';
    el.href = src;
    document.head.appendChild(el);
  };

  App.prototype.validate = {
    email: function (value) {
      return value.match(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    },
    strlen: function (value, min, max) {
      return value.length >= min && value.length <= max;
    },
    isError: function (test, e, params) {
      params = params || [];
      var status = !this[test].apply(null, [e.value].concat(params));
      e.classList.toggle('b-subscribe-modal__input--error', status);
      return status;
    }
  };

  App.prototype.submit = function (e) {
    e.preventDefault();

    var form = this.search('.b-subscribe-modal__form', this.container);

    if(this.validate.isError('strlen', form.name, [1, 40])) {
      return false;
    }

    if(this.validate.isError('email', form.email)) {
      return false;
    }

    var btn = App.prototype.search('.b-subscribe-modal__submit', form);
    btn.classList.add('b-subscribe-modal__submit--loader');
    btn.disabled = 'disabled';

    var formData = new FormData(form);

    var xhr = new XMLHttpRequest();

    xhr.open('POST', '/cms/modules/subscribe/bonus/Bonus.php', true);

    xhr.onreadystatechange = function() {
      if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
        var result = JSON.parse(this.responseText);
        btn.classList.remove('b-subscribe-modal__submit--loader');
        btn.disabled = false;

        [].forEach.call(form.children, function (el) {
          if(el.tagName.toLowerCase() !== 'input' ) return false;
          el.classList.remove('b-subscribe-modal__input--error');
        })

        if(result.status == 'success') {
          var information = App.prototype.search('.b-subscribe-modal-info');
          information.classList.add('b-subscribe-modal-info--visible');
          if(dataLayer) {
            dataLayer.push({'event' : 'bonusWidget'});
          }
          return false;
        }

        Object.keys(result.message).forEach(function (name) {
          form[name].classList.add('b-subscribe-modal__input--error');
        });
      }
    };

    xhr.send(formData);
  };

  App.prototype.open = function (e) {
    var button = e.target.parentElement.tagName.toLowerCase() != 'button' ? e.target : e.target.parentElement;
    var modal = this.container.children[0];

    if(button.classList.contains('b-subscribe-gift__btn--active')) {
      return false;
    }
    
    modal.classList.add('b-subscribe-modal--active');
    button.classList.add('b-subscribe-gift__btn--active');

    this.animate(modal, this.animation, function () {
      this.classList.add('b-subscribe-modal--out');
    });
  };

  App.prototype.close = function () {
    var openButton = this.search('.b-subscribe-gift__btn', this.container);
    var information = this.search('.b-subscribe-modal-info', this.container);
    var modal = this.container.children[0];

    if(!openButton.classList.contains('b-subscribe-gift__btn--active')) {
      return false;
    }
  
    openButton.classList.remove('b-subscribe-gift__btn--active');

    this.animate(modal, this.animation, function () {
      this.classList.remove('b-subscribe-modal--out', 'b-subscribe-modal--active');
      information.classList.remove('b-subscribe-modal-info--visible');
    });
  };

  App.prototype.animate = function (e, name, callback) {
    callback = callback || false;
    var animation = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

    e.classList.add('animated', name);

    var handler = function () {
      e.classList.remove('animated', name);
      if(callback) callback.call(e);
      App.prototype.eventListener(animation, handler, true);
    };

    this.eventListener(animation, handler);
  };

  App.prototype.eventListener = function (handler, callback, remove, dom) {
    dom = dom || window;
    var nameFunc = remove ? 'removeEventListener' : 'addEventListener';

    handler.split(' ').forEach(function (name) {
      dom[nameFunc](name, callback, true);
    });
  };

  App.prototype.search = function (name, dom, lot) {
    dom = dom || document;
    lot = lot && 'querySelectorAll' || 'querySelector';
    return dom[lot](name);
  };

  App.prototype.onBind = function (callbackName, e) {
    return this[callbackName](e);
  };

  App.prototype.eventListener('load', function () {
    if (document.body.clientWidth >= 768) {
      return new App();
    }
  });

  return App;
})();