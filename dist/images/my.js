jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { 
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); 
        }
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { 
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};


function prepareString(str)
{
    outstr = '';
    for (i=0;i<str.length;i++)
    {
        for(j=0;j<10;j++)
        {
            if(str.substr(i,1) === j.toString())
            {
                if((outstr.length == 0 && j > 0)||(outstr.length>0))
                    outstr = outstr + str.substr(i,1);
                break;
            }
        }
    }

    return outstr;
}

function formatNumber(num, offset)
{
    nstr = num.toString();
    ostr = '';

    for(i=(nstr.length-1),j=offset;i>=0;i--,j++)
    {
        if((j%3 == 0) && (j>0)) ostr = ' ' + ostr;
        ostr = nstr.substr(i,1) + ostr;
    }

    return ostr;
}


$(document).ready(function(){
    if( $('div#content').length > 1 )
    {
        var content_down = parseInt($('div#content').position().top) + parseInt($('div#content').css('height'));
        var iframe_h = parseInt($("#sidebar iframe").attr('height'));
        //	$('div.right-block').not(':hidden').last().addClass("last-item");
        $('div.right-block').each(function (index, element) {
            el_top = parseInt($(element).position().top) + parseInt($(element).css('height'));
            //console.log(element);
            //console.log("top: "+parseInt($(element).position().top));
            //console.log("height: "+parseInt($(element).css('height')));
            //console.log(el_top + iframe_h +" "+content_down);
            if (el_top + iframe_h > content_down) {
                $(element).hide();
            }
            else {
                $(element).show();
            }
        });
    }
	
	$('ul.menu li>ul.submenu').parent().mouseenter(function(){
        $('ul.submenu',this).slideDown('fast');

    })
    $('ul.menu li>ul.submenu').parent().mouseleave(function(){
        $('ul.submenu',this).slideUp('fast');
    });

    

    var i = 0;
        $( ".show_text" ).click(function() {
            if (i === 0) {
                $( ".showr-text" ).first().show( "fast", function showNext() {
                    $( this ).next( ".showr-text" ).show( "fast", showNext );
                    i++;
                    return false;       
                });
            }
            else {
                $( ".showr-text" ).hide( 1000 );
                i--;
            };
        }); 
});

