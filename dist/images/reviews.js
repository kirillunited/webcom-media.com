var reviewsService = {
    form: false,
    errorState: false,

    init: function() {
        this.form = $('.reviews-modal__form');

        this.validateFieldsKeyUp();

        $('.reviews-modal').on('hidden.bs.modal', function() {
            var _this = $(this);
            _this.find('.reviews-modal__information').hide();
            _this.find('.reviews-modal__form--information').hide();
        });
    },

    substring: function(_count, container, __this) {
        if (!__this) {
            __this = $(document);
        }
        $(container, __this)
            .removeClass('transparent')
            .removeAttr('style')
            .each(function() {
                var _this = $(this);
                var _text = _this.html();
                var height = _this.height();
                var lineHeight = parseInt(_this.css('lineHeight'));
                var count = height / lineHeight;

                if (count < _count) {
                    return;
                }

                _this
                    .height(lineHeight * _count)
                    .addClass('transparent')
                    .attr('data-height', height);
            });

        $(container + '.transparent', __this).click(function() {
            var _this = $(this);
            var _height = _this.data('height');

            if (!_height) {
                return false;
            }

            if (!_this.hasClass('open')) {
                _this.animate({
                    height: _height
                }, 500, function() {
                    _this.addClass('open');

                    setTimeout(function() {
                        _this.removeClass('transparent')
                            .removeAttr('data-height')
                            .removeAttr('style');
                    }, 200);
                })
            }
        })
    },

    submit: function(_this) {
        _this = $(_this);

        var form = new FormData(_this[0]);
        var rates = _this.find('.reviews-rating__rate span').attr('data-rates');

        if (!reviewsService.validateFields()) {
            return false;
        }

        form.append('rates', rates);
        form.append('lang', reviewsConfig.lang);

        $.ajax({
            url: reviewsConfig.base_url + '/reviews?api_key=' + reviewsConfig.publicToken,
            type: 'POST',
            dataType: 'json',
            data: form,
            contentType: false,
            processData: false,
            crossDomain: true,
            beforeSend: function() {
                _this.parent().find('.reviews-modal__loading').addClass('is-active');
            },
            error: function() {
                _this.parent().find('.reviews-modal__loading').removeClass('is-active');
                console.log('Произошла ошибка отправки данных на сервер');
            },
            success: function(_data) {
                _data = JSON.parse(_data);
                _this.parent().find('.reviews-modal__loading').removeClass('is-active');

                if (!_data.success) {
                    return this.addErrorToFields(_data.message);
                }

                _this.parent().find('.reviews-modal__information').show();
                _this.find('.reviews-rating__rate span').css('width', '0%');
                _this.trigger('reset');
            }.bind(this)
        });

        return false;
    },

    addErrorToFields: function(errors) {
        $.each(errors, function(name, value) {
            if (name.indexOf('other_') != -1) {
                name = name.replace(/^other_([A-z0-9]+)$/i, 'other[$1]');
            }

            var field;

            if (name == 'rates') {
                field = this.form.find('.reviews-rating__rate span');
                this.errorHandler('Пожалуйста оцените наше сотрудничество', field.parent().parent(), true);
                return;
            }

            field = this.form.find('[name="' + name + '"]');
            this.errorHandler(value, field);
        }.bind(this));
    },

    errorHandler: function(message, _this, _no_border) {
        if (!_no_border)
            _this.addClass('reviews-modal__form-input--red');

        _this.parent().append('<span class="reviews-modal__form-helper">' + message + '</span>');
    },

    validateFieldsKeyUp: function() {
        this.form.on('input change focusout', 'input, textarea, select', function() {
            var filters = $(this).data();
            reviewsService.validateField(filters, $(this));
        });
    },

    validateFields: function() {
        this.errorState = false;

        this.form.find('input,textarea,select').each(function() {
            var filters = $(this).data();
            reviewsService.validateField(filters, $(this));
        });

        var rates = this.form.find('.reviews-rating__rate span');
        if (rates.attr('data-rates') == undefined) {
            this.errorState = true;
            this.errorHandler('Пожалуйста оцените наше сотрудничество', rates.parent().parent(), true);
        } else {
            rates.parent().parent().parent().find('.reviews-modal__form-helper').remove();
        }

        return !this.errorState;
    },

    validateField: function(filters, _this) {
        var error = false;

        if (_this.hasClass('reviews-modal__form-input--red')) {
            _this.removeClass('reviews-modal__form-input--red');
            _this.parent().find('.reviews-modal__form-helper').remove();
        }

        $.each(filters, function(filter, value) {
            var result = this.typeValidate(filter, value, _this.val());

            if (result == null || result == true) {
                return;
            }

            if (error != false) {
                return false;
            }

            error = result;
        }.bind(this));

        if (error) {
            this.errorState = true;
            this.errorHandler(error, _this);
        }
    },

    typeValidate: function(filter_name, filter_value, value) {
        if (!filter_value) {
            return null;
        }

        switch (filter_name) {
            case 'required':
                return value.length < 1 ? 'Это обязательное поле' : true;
            case 'maxStrlen':
                return (value.length > filter_value) ? 'Вы привысили допустимый лимит в ' + filter_value + ' символов' : true;
        }
    },

    /**
     * @param count - кол-во звезд
     * @param padding - погрешность в px
     */
    stars: function(count, padding) {
        var container = this.form.find('.reviews-rating__rate');
        var rates = 0;
        var active = 0;

        container.mousemove(function(e) {
            var _this = $(this);
            var width = _this.width();
            var percent = (((e.pageX - _this.offset().left + 5) * 100) / width);
            var starWidth = (width / count) / width;
            var countHalf = ((starWidth * 100) / 2);
            var diff = (percent / countHalf).toFixed() * countHalf;
            diff -= (padding * 100) / width;

            if (diff <= active) {
                return false;
            }

            rates = (percent / 10).toFixed();
            _this.find('span').css('width', diff + '%');
        }).mouseout(function() {
            var _this = $(this).find('span');
            var static = _this.attr('style').substr(7).slice(0, -2);

            if (!active) {
                _this.css('width', '0%');
            } else if (active <= static) {
                _this.css('width', active + '%');
            }
        });

        container.click(function() {
            var _this = $(this).find('span');
            _this.attr('data-rates', rates);
            _this.parent().parent().parent().find('.reviews-modal__form-helper').remove();
            active = _this.attr('style').substr(7).slice(0, -2);
        });
    }
};
