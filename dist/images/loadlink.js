$(window).load(function(){
  $(".loadlink").each(function(){
	$(this).replaceWith('<a href="'+$(this).attr("data-link")+'" ' + ($(this).attr("onclick") != null ? 'onclick="'+$(this).attr("onclick")+'"'  : '') + ($(this).attr("target") != null ? 'target="'+$(this).attr("target")+'"'  : '') + '>'+$(this).html()+'</a>');	 
  });
  $(".langlink").each(function(){
	$(this).replaceWith('<a href="'+$(this).attr("data-link")+'" class="lang" id="'+$(this).attr("id") + '">' + $(this).html() + '</a>');
	});
});