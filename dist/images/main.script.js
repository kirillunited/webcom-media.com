(function($) {
    'use strict';

    var $window = $(window),
        $body = $('body');

    var fancyBox = function() {
        $(".fancybox").fancybox({
            maxWidth: 800,
            maxHeight: 600,
            fitToView: false,
            width: '70%',
            height: '70%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });

        $(".fancybox_pdf").click(function() {
            $.fancybox({
                maxWidth: 1050,
                width: '100%',
                type: 'html',
                autoSize: false,
                content: '<embed src="' + this.href + '#nameddest=self&page=1&view=FitH,0&zoom=80,0,0" type="application/pdf" height="99%" width="100%" />',
                beforeClose: function() {
                    $(".fancybox-inner").unwrap();
                },
                beforeShow: function(){
                  console.log($('.fancybox-inner').find('embed').width());
                }
            });
            return false;
        });
    };

    $(document).ready(function() {
        fancyBox();
    });
})(jQuery);
