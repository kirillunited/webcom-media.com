/**
 * Created by M.Gerasimov on 22.07.2016.
 */
$(function () {
    var generalSlider = $('.owl-carousel.general-slider');

    var sliderItem = generalSlider.find('.item');

    var slider_loop = false;
    var slider_nav = false;
    var slider_dots = false;
    var slider_mouseDrag = false;

    if( sliderItem.length > 1 ) {
        slider_loop = true;
        slider_nav = true;
        slider_dots = true;
        slider_mouseDrag = true;
    }

    generalSlider.owlCarousel({
        loop: slider_loop,
        nav: slider_nav,
        dots: slider_dots,
        mouseDrag: slider_mouseDrag,
        autoplay: true,
        margin: 10,
        items: 1,
        onInitialize: function() {
            var style = $('<style>').prop('type', 'text/css')
                .insertAfter(generalSlider);

            sliderItem.each(function(i) {
                var _this = $(this);
                var color1 = _this.data('color1');
                var color2 = _this.data('color2');
                var image  = _this.data('image');

                if( image ) {
                    _this.css({'backgroundImage': 'url('+image+')'});
                }

                if( color1 && color2 ) {
                    var normal = color1.split('|');
                    var hover  = color2.split('|');
                    var line = 58;

                    data = '.owl-carousel.general-slider .item a.itemid'+i+' {background:-webkit-linear-gradient(top,#' + normal[0] + ' '+line+'%,#' + normal[1] + ');'
                        +'background:-moz-linear-gradient(top,#' + normal[0] + ' '+line+'%,#' + normal[1] + ');'
                        +'background:-ms-linear-gradient(top,#' + normal[0] + ' '+line+'%,#' + normal[1] + ');'
                        +'background:-o-linear-gradient(top,#' + normal[0] + ' '+line+'%,#' + normal[1] + ');'
                        +'background:linear-gradient(to bottom,#' + normal[0] + ' '+line+'%,#' + normal[1] + ');}\n';
                    data += '.owl-carousel.general-slider .item a.itemid'+i+':hover {background:-webkit-linear-gradient(top,#' + normal[0] + ' '+line+'%,#' + normal[1] + ');'
                        +'background:-moz-linear-gradient(top,#' + hover[0] + ' '+line+'%,#' + hover[1] + ');'
                        +'background:-ms-linear-gradient(top,#' + hover[0] + ' '+line+'%,#' + hover[1] + ');'
                        +'background:-o-linear-gradient(top,#' + hover[0] + ' '+line+'%,#' + hover[1] + ');'
                        +'background:linear-gradient(to bottom,#' + hover[0] + ' '+line+'%,#' + hover[1] + ');}\n';

                    style.append(data);
                    _this.find('a').addClass('itemid'+i);
                }
            });
        }
    });
});