function MyCarousel(name, options) {
    var $name = name;
    var $options = {
        num: 5,
        step: 1
    };
    var $el = $('#'+$name);

    $options.from = 0;
    $options.count = $el.children().length;
    $.extend($options, options);
    $options.to = $options.from + $options.num - 1;

    run();

    $('#'+$name+'-next').click(function() {
        if ($options.to+$options.step >= $options.count) {
            if (!$options.nextOnly) return false;
            $options.from = 0;
            $options.to = $options.num-1;
        } else {
            $options.from += $options.step;
            $options.to += $options.step;
        }
        run();
        return false;
    });
    $('#'+$name+'-prev').click(function() {
        if ($options.from-1 < 0) return false;
        $options.from -= $options.step;
        $options.to -= $options.step;
        run();
        return false;
    });
    function run () {
        $el.children().each(function(i) {
            if (i < $options.from || i > $options.to) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    }
}