$(function() {
	var $body = $('body');

	var clickBlock = function() {
		$body.find('.news-onas-item__border').addClass('news-onas-item__border--hover');
		$body.find('.news-onas-item__full').css({'pointerEvents': 'none'});

		$body.find('.news-onas-item').click(function() {
			var url = $(this).find('.news-onas-item__full').attr('href');
			window.open(url,'_blank');
		});
	};

	/* AJAX ��������� ���������� */
	var pagination = function() {
		var ias = $.ias({
			container: '.news-onas-list',
			item: '.news-onas-item',
			pagination: '.content-footer',
			next: '.content-footer__pagination a.next',
		});

		ias.extension(new IASSpinnerExtension({
		    html: '<div style="clear:both"></div><div class="content-footer__loader" title="�������� �������� �������"></div>'
		}));

		ias.on('rendered', clickBlock);
	};

	clickBlock();
	pagination();
});