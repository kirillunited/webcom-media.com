$(document).ready(function(){
    var wmnsapi_options = [
        // Organic
        {medium: 'organic', source: 'google', code: 'GO'},
        {medium: 'organic', source: 'yandex', code: 'YO'},
        {medium: 'organic', source: 'go.mail.ru', code: 'GM'},
        {medium: 'organic', source: 'search.tut.by', code: 'ST'},
        {medium: 'organic', source: 'nova.rambler.ru', code: 'NR'},
        {medium: 'organic', source: 'bing', code: 'BI'},
        {medium: 'organic', source: 'yahoo', code: 'YH'},

        // Yandex Direct
        {medium: 'cpc', source: 'yandex', code: 'YD'},

        // Google AdWords
        {medium: 'cpc', source: 'google', code: 'GA'},

        // Target Mail.ru
        {medium: 'cpc', source: 'target', code: 'TG'},

        // Socials
        {medium: 'social', source: 'facebook.com', code: 'FB'},
        {medium: 'social', source: 'vk.com', code: 'VK'},
        {medium: 'social', source: 'odnoklassniki', code: 'OK'},

        // Direct
        {medium: '(none)', source: '(direct)', code: 'DR'}
    ];

    WMNSAPI.cookie_name = 'amoCookie';
    WMNSAPI.track(wmnsapi_options);

    WMNSAPI.cookie_name = 'amoCookieLast';
    WMNSAPI.cookie_type = 'last_source';
    WMNSAPI.track(wmnsapi_options);
});

