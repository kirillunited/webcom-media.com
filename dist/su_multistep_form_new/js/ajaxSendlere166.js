/*
 *   Поля формы:
 *   name - Имя/Представьтесь, пожалуйста
 *   phone - Телефон
 *   email - E-mail
 *   site - Адрес сайта
 *   time - Время звонка
 *   text - Комментарий
 *   service - Название услуги (тип аудита)
 *   id - Идентификатор формы (alias страницы, с которой отправлена форма) - заполняется автоматически
 *   title - Заголовок формы (содержимое тега h1 страницы, с которой отправлена форма) - заполняется автоматически
 *
 *   Структура шаблона формы:
 *   <form data-tpl-id="N">
 *   N - id почтового шаблона
 *       <input type="text" name="name" placeholder="Иван Иванов" required="">
 *       значение аттрибута name="" - тип пользовательских данных / тип валидации поля
 *       required="" - атрибут устанавливает, что поле обязательное и будет валидироваться по правилу из поля name=""
 *       .ajaxSendler_btn - элемент, по клику на который происходит обработка формы
 *   </form>
 * */

 /*
    Что касается меток: при добавлении новой метки на страницу. Не забываем указать ее в конфиге со стороны сервера
 */
jQuery(document).ready(function ($)
{
    $("input[name=time]").inputmask("99:99");

    var btn = $('.ajaxSendler_btn');
    var form = btn.closest('form');

    form.on("input change", "input[required], textarea[required], select[required]", function ()
    {
        if (validateField($(this).attr('name'), $(this)))
        {
            $(this).removeClass('invalid');
        }
        else
        {
            $(this).addClass('invalid');
        }
    });

    btn.on('click', function (e)
    {
        e.preventDefault();
        var $this = $(this);

        var form = $this.closest('form');
        // console.log(form);

        if (isValidForm(form))
        {
            var formData = new FormData(form[0]);
            var title = form.data('title');

            if( title == undefined ) {
            	title = ($('h1').length > 0) ? $('h1').text() : $('title').text();
            }

            var host = "https://www.promo-webcom.by";
            var str = window.location.pathname;
            var id = form.data('form-id');

            if( id == undefined ) {
                id = str.substr(0, str.length - 1).split('/').pop();
            }

            var tpl_id = form.data('tpl-id');

            // if(window.location.hostname == "www.pay.webcom-media.by") // для pay.webcom-media.by , чтобы избежать Cross-Domain scripting и правильно выставить id формы
            // {
            //     if( id == false ) {
            //         id = "samoupravlenie";
            //     }
            //     host = "https://www.pay.webcom-media.by";
            // }

            if(form.attr('id') == "axs-call")
            {
                formData.append('id', id + "_call");
            }
            else
            {
                formData.append('id', id);
            }

            formData.append('page_title', title);
            formData.append('tpl_id', tpl_id);

            if($(form).find('input[name=name]').val() == "tp_test")
            {
                apitest(formData, host, id);
                return;
            }

            $.ajax({
                url:  host + '/cms/modules/ajaxSendler/ajaxSendler.php',
                type: 'POST',
                data: formData,
                beforeSend: function ()
                {
                    doCustomBeforeSend.call($this, id, form);
                },
                success:    function (result)
                {
                    var resp = JSON.parse(result);
                    if (resp.success)
                    {
                        doCustomSuccess.call($this, id, form);
                        form[0].reset();

                        formData.append('type', 'amoTestUnsorted');
		                    formData.append('amoCookie', getCookie('amoCookie'));
		                    formData.append('amoCookieLast', getCookie('amoCookieLast'));
                        // Отправляем заявку в amoCRM
                        $.ajax({
                            url:  host+"/cms/api/amoAPI.php",
                            type: 'POST',
                            data: formData,
                            cache:       false,
                            contentType: false,
                            processData: false
                        });

                        // Очень важно - отправляет событие успешной отправки формы в аналитику
                        dataLayer.push({'event' : id + "_Order"});
                    }
                },
                error:       function ()
                {
                    doCustomError.call($this, id, form);
                },
                cache:       false,
                contentType: false,
                processData: false
            });
        }
        else
        {
            if( form.data('popup') ) {
                form.find('.invalid:first').focus();
                return false;
            }

            // Scroll to first invalid field for all forms, except "b-new-modal__form"
            if (form.hasClass('b-new-modal__form')) {
                form.find('.invalid:first').focus();
            } else {
                $('html, body').animate({
                    scrollTop: form.find('.invalid:first')
                        .offset()
                        .top - 200
                }, 1000, function ()
                {
                    form.find('.invalid:first').focus();
                });
            }
        }
        return false;
    });

	function getCookie(name) {
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2) return parts.pop().split(";").shift();
	}

    function apitest(formData, host, id)
    {
        formData.append('type', 'amoTestUnsorted');
        formData.append('amoCookie', getCookie('amoCookie'));
        formData.append('amoCookieLast', getCookie('amoCookieLast'));
        // Отправляем заявку в amoCRM
        /*$.ajax({
            url:  host+"/cms/api/amoAPI.php",
            type: 'POST',
            data: formData,
            cache:       false,
            contentType: false,
            processData: false
        });*/
		alert(id);
		dataLayer.push({'event' : id + "_Order"});
    }

    function validateField(fieldName, elem)
    {
        switch (fieldName)
        {
            case 'name':
                if ($(elem).val().length < 2 || !isValidFIO($(elem).val()))
                {
                    return false;
                }
                break;
            case 'phone':
                if (!isValidPhoneNumber($(elem).val()))
                {
                    return false;
                }
                break;
            case 'email':
                if (!isValidEmailAddress($(elem).val()))
                {
                    return false;
                }
                break;
            case 'site':
                if (!isValidSiteAddress($(elem).val()))
                {
                    return false;
                }
                break;
            case 'text':
                if ($(elem).val().length === 0)
                {
                    return false;
                }
                break;
            case 'time':
                if (!isValidTime($(elem).val()) || !correctTime($(elem).val()))
                {
                    return false;
                }
                break;
            case 'service':
                if ($(elem).val() === "выбрать")
                {
                    return false;
                }
                break;
        }
        return true;
    }

    function isValidFIO(fio)
    {
        var pattern = new RegExp(/^([а-яА-ЯёЁa-zA-Z])/i);
        return pattern.test(fio);
    }

    function isValidPhoneNumber(phone)
    {//\d{3}([ .-])?\d{3}([ .-])?\d{4}|\(\d{2,3}\)([ ])?\d{3}([.-])?\d{4}|\(\d{3}\)([ ])?\d{3}([ ])?\d{4}|\(\d{3}\)([.-])?\d{3}([.-])?\d{4}|\d{3}([ ])?\d{3}([ .-])?\d{1,3}
        var patternBel = new RegExp(/^(\+?(375|8)[\s]?)(\(?(17|162|163|1644|165|1655|212|2151|2156|2157|214|232|2334|2351|2342|152|1512|1561|1592|1514|177|1771|1772|1775|1773|1795|174|1717|222|2232|225|2231|2233|2248|2237|2244|2236|2245|2238|2241|2234|2240|2235|2246|2247|2242|2243|2239|33|44|25|29|0(17|162|163|1644|165|1655|212|2151|2156|2157|214|232|2334|2351|2342|152|1512|1561|1592|1514|177|1771|1772|1775|1773|1795|174|1717|222|2232|225|2231|2233|2248|2237|2244|2236|2245|2238|2241|2234|2240|2235|2246|2247|2242|2243|2239|25|29|33|44))\)?[\s]?)(\d{2,3}[-\s]?)(\d{2}[-\s]?){2}$/i),
            patternRus = new RegExp(/^(\+?(7)[\s]?)(\(?\d{3}\)?[\s]?)(\d{3}[-\s]?)(\d{2}[-\s]?){2}$/i),
            patternOther = new RegExp(/^(\+\d)/i);

        if (!phone.indexOf("+375") || !phone.indexOf("375") || !phone.indexOf("80")) {
            return patternBel.test(phone);
        }
        else if (!phone.indexOf("+7") || !phone.indexOf("7")) {
            return patternRus.test(phone);
        }
        else if (!phone.indexOf("+")) {
            return patternOther.test(phone);
        }
    }

    function isValidEmailAddress(emailAddress)
    {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }

    function isValidSiteAddress(siteAddress)
    {
        var pattern = new RegExp(/^(([а-яёa-z0-9]{1})((\.[а-яёa-z0-9-])|([а-яёa-z0-9-]))*\.([а-яёa-z]{2,7})(\/?)$)/i);
        return pattern.test(siteAddress);
    }

    function isValidTime(time)
    {
        var pattern = new RegExp(/^((0?[0-9])|(1[0-9])|(2[0-3])):((0?[0-9])|([1-5][0-9]))$/);
        return pattern.test(time);
    }

    function correctTime(time)
    {
        var today = new Date();
        var year = today.getFullYear().toString();
        var month = (today.getMonth() + 1).toString();
        var mnt = ('0' + month).slice(-2);
        var date = today.getDate().toString();
        var userDate = Date.parse(mnt + " " + date + ", " + year + " " + time);
        var systemDate = today.getTime();
        return userDate > systemDate;
    }

    function isValidForm(form)
    {
        var isValidForm = true;
        $.each(form.find('input[required], textarea[required], select[required]'), function ()
        {
            if ( ! validateField($(this).attr('name'), $(this)) )
            {
                $(this).addClass('invalid');
                isValidForm = false;
            }
        });
        return isValidForm;
    }

    function doCustomBeforeSend(formId, form)
    {
        switch (formId)
        {
            case 'orderform':
            case 'antikrizisnoe-reshenie':
                form.find('.send-button').css('background', 'none');
                form.find('.ajaxSendler_btn').replaceWith('<div class="axs-loader"></div>');
                break;
            case '11165-xotite-poluchit-novyih-klientov-besplatno-sprosite-u-nas':
                $('.goole_news__form-send').hide();
                $('.goole_news__form_loader').show();
                break;
            case 'google':
                $('.block7__form-send').css('background', '#fff');
                $('.block7__form_loader').show();
                break;
            case 'yandex-direct':
                $('.button_yan').css('visibility', 'hidden');
                $('.questions_yandex_form .form_loader').show();
                break;
            // case 'samoupravlenie':
            case 'su':
            case 'offer-condition':
                $('#bottomForm .preloaders').show();
                break;
            case 'samoupravlenie-premium':
                $('#formPremium .preloaders').show();
                break;

            // NEW LandingPage
            // Socials
            case 'targeted-footer':
            case 'targeted-offer':
                //Стереть, после выпуска страницы
                $('.question-form form').hide();
                $('.question-form__loader').show();

                $('.b-feedback__information').removeAttr('style');
                $('.b-feedback__loader').show();
            break;

            case 'targeted-popup':
                $('#reviewsOpen .type-modal__form').hide();
                $('#reviewsOpen .type-modal__loader').show();
            break;
            //Cashback
            case 'vozvrat_deneg_za_seo':
                $('.question-form form').hide();
                $('.question-form__loader').show();
            break;
            //Free-Audit-SEO/Context
            case 'free-audit':
            case 'free-audit-context':
                $('.audit-form__btn, .audit-form__wrap-item').hide();
                $('.audit-form__loader').show();
            break;
            //Context-Advert, Mobile-apps-ad page
            case 'context-advert':
            case 'seo-page':
            case 'reklama-mobilnyh-prilozhenij':
            case 'paketnye-predlozheniya':
            case 'prodvizhenie-sajta-po-poziciyam':
            case 'prodvizhenie-sajta-s-oplatoj-za-chasy':
            case 'prodvizhenie-s-oplatoj-za-lidy':
            case 'prodvizhenie-sajta-po-trafiku':
            case 'prodvizhenie-molodyih-sajtov':
            case 'test-drive':
            case 'text-youtube':
            case 'text-instagram':
            case 'tourism':
            case 'cases':
                $('.b-feedback__information').removeAttr('style');
                $('.b-feedback__loader').show();
                break;

            // Webcom International
            case 'webcom-international':
                $('[data-form-id="' + formId + '"] .b-form__loader').show();
                break;


            // Mtbank-new
            case 'mtbank-free-consult':
                console.log('mtbank-free-consult before send');
                // console.log(form);
                if (form.length > 0) {
                    $('.b-new-modal .b-form__success').hide();
                    $('.b-new-modal .b-form__error').hide();
                    $('.b-new-modal .b-form__fields').hide();
                	$('.b-new-modal .b-form__information').addClass('b-form__information--visible');
                    $('.b-new-modal .b-form__loader').show();
                }
                break;

            case 'mtbank-partner-form':
                // console.log(form);
                if (form.length > 0) {
                    $('.b-map .b-form__success').hide();
                    $('.b-map .b-form__error').hide();
                    $('.b-map .b-form__btn-wrapper').addClass('b-form__btn-wrapper--hidden');
                    $('.b-map .b-form__information').removeClass('b-form__information--error').addClass('b-form__information--visible');
                    $('.b-map .b-form__loader').show();
                }
                break;



                //BonusVK
                //Videoreklama
                //Samoupr
                //Mobile Apps AD
                //Remarketing
                //Mobile-Advert
                //Construction
                //Lawyer
                //Cafe
                //Ok
                //Yandex Market
                //Vk
                //KP
            case 'kp':
            case 'vk':
            case 'y-market':
            case 'e-commerce':
            case 'ok':
            case 'cafe':
            case 'beauty-salon':
            case 'lawyer':
            case 'construction':
            case 'mobile-advert':
            case 'remarketing':
            case 'samoupravlenie':
            case 'videoreklama':
            case 'kr-packages':
            case 'bonusvk':
            case 'mobileapps':
                $('.b-feedback__form').hide();
                $('.b-feedback__information').show();
                $('.b-feedback__loader').show();
                break;
            //Contacts
            //Facebook
            //Online-request
            //Web-Analytics
            //Instagram-new
			//Remarketing-new
            //Context-Advert-new
            //Paketnye-new
			//Youtube-new
            //Sy-new
            //Target-advert-new
            //Videoreklama-new
            //KP-new
            //search-optimtzation-new
            //Cost-Context-Advert
            //Context-Advert-in-Yandex
            //Context-Advert-in-Google
            //SEO-audit
            //KR-audit
            //Complex-audit
            //Technical-audit
            //Marketing-audit
            //Audit
            //Moneyback
            //SEO-Cost
            //Market
            //Medijnaya-reklama
            //Protext
            //Index page
            //New Year page
            case 'new-year':
            case 'index':
            case 'okna-mebel':
            case 'auto-dealer':
            case 'stroika-new':
            case 'tourism-new':
            case 'salon-krasoty':
            case 'medicina':
            case 'yuridicheskie-uslugi':
            case 'nedvizhimost':
            case 'banki-strahovanie':
            case 'site-promition':
            case 'cafe-new':
			case 'tech-support':
			case 'pro-text':
			case 'medijnaya-reklama':
            case 'vibor':
            case 'sponsorship':
            case 'partner-programm':
            case 'market':
            case 'action-webcom-vk':
            case 'company':
            case 'seo-cost':
            case 'moneyback':
            case 'audit':
            case 'marketing-audit':
            case 'technical-audit':
            case 'complex-audit':
            case 'kr-audit':
            case 'seo-audit':
            case 'kr-google':
            case 'kr-yandex':
            case 'kr-cost':
			case 'search-optimtzation-new':
            case 'kp-new':
            case 'videoreklama-new':
			case 'target-advert-new':
			case 'paketnye-new':
			case 'sy-new':
			case 'youtube-new':
			case 'instagram-new':
            case 'context-new-advert':
            case 'remarketing-new':
            case 'web-analytics':
            case 'ok-new':
            case 'vk-new':
            case 'mobile-new':
            case 'e-commerce-new':
            case 'kr-search':
            case 'seo-new':
            case 'audit-new':
            case 'consalting':
            case 'advert-mobile-new':
            case 'test-drive-new':
            case 'facebook':
            case 'y-market-new':
            case 'contacts':
                $('.b-form__fields').hide();
                $('.b-form__information').show();
                $('.b-form__loader').show();
                break;
            //Online-request
            //Online-call
            case 'online-request':
                $('[data-form-id="online-request"] .b-form__fields').css({'visibillity':'hidden', 'opacity':'0'});
                $('[data-form-id="online-request"] .b-form__information').show();
                $('[data-form-id="online-request"] .b-form__loader').show();
                break;
            case 'online-call':
                $('[data-form-id="online-call"] .b-form__fields').hide();
                $('[data-form-id="online-call"] .b-form__information').show();
                $('[data-form-id="online-call"] .b-form__loader').show();
                break;
        }
    }

    function doCustomSuccess(formId, form)
    {
        switch (formId)
        {
            case 'orderform':
            case 'antikrizisnoe-reshenie':
                form.find('.axs-loader').css('visibility', 'hidden');
                form.find('.axs-thx').css('visibility', 'visible');
                form.parent().siblings('.axs-block').hide();
                form.parent().find('p').hide();
                form.parent().css('margin', '0');
                break;
            case '11165-xotite-poluchit-novyih-klientov-besplatno-sprosite-u-nas':
                $('.goole_news__form_loader').hide();
                $('.goole_news_typ').show();
                break;
            case 'google':
                $('.block7__form_loader').hide();
                $('.block7__typ').show();
                break;
            case 'yandex-direct':
                $('.questions_yandex_typ').show();
                $('.questions_yandex_form .form_loader').hide();
                break;
            // case 'samoupravlenie':
            case 'su':
            case 'offer-condition':
                $('#bottomForm .order_form-wr').css('opacity', '0');
                $('#bottomForm .order_form-ok').show();
                $('#bottomForm .preloaders').hide();
                $('#bottomForm .order_form--btn').css('opacity', '0');
                $('.order_form .header h3').css('opacity', '0');
                break;
            case 'samoupravlenie-premium':
                $('#formPremium .header').hide();
                $('#formPremium .popup_form form').hide();
                $('#formPremium .popup_form .order_form-ok').show();
                $('#formPremium .preloaders').hide();
                break;

            // NEW LandingPage
            // Socials
            case 'targeted-footer':
            case 'targeted-offer':
                $('.question-form__loader').hide();
                $('.question-form__success').show();

                $('.b-feedback__loader').hide();
                $('.b-feedback__success').show();
            break;

            case 'targeted-popup':
                $('#reviewsOpen .type-modal__title').hide();
                $('#reviewsOpen .type-modal__loader').hide();
                $('#reviewsOpen .type-modal__information').show();
            break;
            //Cashback
            case 'vozvrat_deneg_za_seo':
                $('.question-form p, .question-form__loader, .question-form h2').hide();
                $('.question-form__success').show();
            break;
            //Free-Audit-SEO/Context
            case 'free-audit':
            case 'free-audit-context':
                $('.audit-form__loader').hide();
                $('.audit-form__success').show();
            break;
            //Context-Advert, Mobile-apps-ad
                //Mobile-Advert
                //Construction
                //Lawyer
                //Cafe
                //Ok
                //Yandex Market
                //Vk
                //KP
                case 'kp':
                case 'vk':
                case 'y-market':
                case 'e-commerce':
                case 'ok':
                case 'cafe':
                case 'beauty-salon':
                case 'lawyer':
                case 'construction':
                case 'kr-packages':
                case 'mobile-advert':
                case 'context-advert':
                case 'seo-page':
                case 'reklama-mobilnyh-prilozhenij':
                case 'paketnye-predlozheniya':
                case 'prodvizhenie-sajta-po-poziciyam':
                case 'prodvizhenie-sajta-s-oplatoj-za-chasy':
                case 'prodvizhenie-s-oplatoj-za-lidy':
                case 'prodvizhenie-sajta-po-trafiku':
                case 'prodvizhenie-molodyih-sajtov':
                case 'test-drive':
                case 'text-youtube':
                case 'text-instagram':
                case 'tourism':
                case 'videoreklama':
                case 'samoupravlenie':
                case 'cases':
                case 'mobileapps':
                case 'remarketing':
                    $('.b-feedback__loader').hide();
                    $('.b-feedback__success').fadeIn();
                    break;

                // Webcom International
                case 'webcom-international':
                    $('[data-form-id="' + formId + '"] .b-form__loader').hide();
                    $.fancybox.open({
                        src: '#b-contacts-result--success'
                    });
                    break;



                // Mtbank-new
                case 'mtbank-free-consult':
                    console.log('mtbank-free-consult succes send');
                    // console.log(form);
                    if (form.length > 0) {
                        console.log('mtbank-free-consult succes send');

                        $('.b-new-modal .b-form__loader').hide();
                        $('.b-new-modal .b-form__error').hide();
                        $('.b-new-modal .b-form__fields').hide();
                        $('.b-new-modal .b-form__information').addClass('b-form__information--visible');
                        $('.b-new-modal .b-form__success').fadeIn();
                    }
                    break;

                case 'mtbank-partner-form':
                    // console.log(form);
                    if (form.length > 0) {
                        console.log('mtbank-partner-form succes send');

	                	$('.b-map .b-form__loader').hide();
	                    $('.b-map .b-form__error').hide();
	                    $('.b-map .b-form__btn-wrapper').addClass('b-form__btn-wrapper--hidden');
                		$('.b-map .b-form__information').removeClass('b-form__information--error').addClass('b-form__information--visible');
	                    $('.b-map .b-form__success').fadeIn();
                    }
                    break;



                    //BonusVK
                case 'bonusvk':
                    $('.b-feedback__loader').hide();
                    $('.b-feedback__success').addClass('active');
                    break;
                //Contacts
                //Facebook
                //Web-Analytics
                //Remarketing-new
				//Instagram-new
                //Context-Advert-new
                //Youtube-new
                //Sy-new
                //Paketnye-new
                //Target-advert-new
                //Videoreklama-new
                //KP-new
                //search-optimtzation-new
                //Cost-Context-Advert
                //Context-Advert-in-Yandex
                //Context-Advert-in-Google
                //SEO-audit
                //KR-audit
                //Complex-audit
                //Technical-audit
                //Marketing-audit
                //Audit
                //Moneyback
                //SEO-Cost
                //Market
                //Medijnaya-reklama
                //Protext
                //Index page
                //New Year page
                case 'new-year':
                case 'index':
                case 'okna-mebel':
                case 'auto-dealer':
                case 'stroika-new':
                case 'tourism-new':
                case 'salon-krasoty':
                case 'medicina':
                case 'yuridicheskie-uslugi':
                case 'nedvizhimost':
                case 'banki-strahovanie':
                case 'site-promition':
                case 'cafe-new':
                case 'tech-support':
			    case 'pro-text':
				case 'medijnaya-reklama':
                case 'vibor':
                case 'sponsorship':
                case 'partner-programm':
                case 'market':
                case 'action-webcom-vk':
                case 'company':
                case 'seo-cost':
                case 'moneyback':
                case 'audit':
                case 'marketing-audit':
                case 'technical-audit':
                case 'complex-audit':
                case 'kr-audit':
                case 'seo-audit':
                case 'audit-new':
                case 'kr-google':
                case 'kr-yandex':
                case 'kr-cost':
				case 'search-optimtzation-new':
                case 'kp-new':
                case 'videoreklama-new':
			    case 'target-advert-new':
				case 'paketnye-new':
                case 'sy-new':
				case 'youtube-new':
				case 'instagram-new':
                case 'context-new-advert':
                case 'remarketing-new':
                case 'web-analytics':
                case 'facebook':
                case 'ok-new':
                case 'vk-new':
                case 'mobile-new':
                case 'seo-new':
                case 'consalting':
                case 'advert-mobile-new':
                case 'test-drive-new':
                case 'e-commerce-new':
                case 'y-market-new':
                case 'kr-search':
                case 'contacts':
                    $('.b-form__loader').hide();
                    $('.b-form__success').fadeIn();
                    break;
                //Online-request
                //Online-call
                case 'online-request':
                    $('[data-form-id="online-request"] .b-form__loader').hide();
                    $('[data-form-id="online-request"] .b-form__success').css({'display':'flex'});
                    break;
                case 'online-call':
                    $('[data-form-id="online-call"] .b-form__loader').hide();
                    $('[data-form-id="online-call"] .b-form__success').fadeIn();
                    break;
        }
    }

    function doCustomError(formId, form)
    {
        console.log('Error put api AMO');
        switch (formId)
        {
            case 'samoupravlenie-premium':
                $('#formPremium .preloaders').hide();
                break;
            // case 'samoupravlenie':
            case 'su':
            case 'offer-condition':
                $('#bottomForm .preloaders').hide();
                break;

            // NEW LandingPage
            // Socials
            case 'targeted-footer':
            case 'targeted-offer':
                $('.question-form form').show();
                $('.question-form__success').hide();

                $('.b-feedback__loader').hide();
                $('.b-feedback__information').hide();
            break;

            case 'targeted-popup':
                $('#reviewsOpen .type-modal__title').show();
                $('#reviewsOpen .type-modal__form').show();
                $('#reviewsOpen .type-modal__loader').hide();
            break;
            //Cashback
            case 'vozvrat_deneg_za_seo':
                $('.question-form form, .question-form p').show();
                $('.question-form__loader').hide();
            break;
            //Free-Audit-SEO/Context
            case 'free-audit':
            case 'free-audit-context':
                $('.audit-form__loader').hide();
                $('.audit-form__btn, .audit-form__wrap-item').show();
            break;
            //Context-Advert, mobile-apps-ad
                //Mobile-Advert
                //Construction
                //Lawyer
                //Cafe
                //Ok
                //Yandex Market
                //Vk
                //KP
                case 'kp':
                case 'vk':
                case 'y-market':
                case 'e-commerce':
                case 'ok':
                case 'cafe':
                case 'beauty-salon':
                case 'lawyer':
                case 'construction':
                case 'mobile-advert':
                case 'context-advert':
                case 'seo-page':
                case 'mobile-apps-ad':
                case 'test-drive':
                case 'text-youtube':
                case 'text-instagram':
                case 'tourism':
                case 'videoreklama':
                case 'samoupravlenie':
                case 'remarketing':
                case 'kr-packages':
                case 'cases':
                    $('.b-feedback__loader').hide();
                    $('.b-feedback__information').hide();
                    break;

                // Webcom International
                case 'webcom-international':
                    $('[data-form-id="' + formId + '"] .b-form__loader').hide();
                    $.fancybox.open({
                        src: '#b-contacts-result--error'
                    });
                    break;

                // Mtbank-new
                case 'mtbank-free-consult':
                    console.log('mtbank-free-consult error send');
                    // console.log(form);
                    if (form.length > 0) {
                        console.log('mtbank-free-consult error send');

                        $('.b-new-modal .b-form__loader').hide();
                        $('.b-new-modal .b-form__success').hide();
                        $('.b-new-modal .b-form__fields').show();
                        $('.b-new-modal .b-form__information').addClass('b-form__information--visible');
                        $('.b-new-modal .b-form__error').fadeIn();
                    }
                    break;

                case 'mtbank-partner-form':
                    // console.log(form);
                    if (form.length > 0) {
                        console.log('mtbank-partner-form error send');

                        $('.b-map .b-form__loader').hide();
                        $('.b-map .b-form__success').hide();
                        $('.b-map .b-form__btn-wrapper').removeClass('b-form__btn-wrapper--hidden');
                    	$('.b-map .b-form__information').addClass('b-form__information--visible').addClass('b-form__information--error');
                        $('.b-map .b-form__error').fadeIn();
                    }
                    break;



                //Contacts
                //Facebook
                //Ok-new
                //Web-Analytics
                //Remarketing-new
                //Context-Advert-new
				//Instagram-new
				//Paketnye-new
                //Youtube-new
                //Sy-new
                //Target-advert-new
                //Videoreklama-new
                //KP-new
                //search-optimtzation-new
                //Cost-Context-Advert
                //Context-Advert-in-Yandex
                //Context-Advert-in-Google
                //SEO-audit
                //KR-audit
                //Complex-audit
                //Technical-audit
                //Marketing-audit
                //Audit
                //Moneyback
                //SEO-Cost
                //Market
                //Medijnaya-reklama
                //Protext
                //Index page
                //New Year page
                case 'new-year':
                case 'index':
                case 'okna-mebel':
                case 'auto-dealer':
                case 'stroika-new':
                case 'tourism-new':
                case 'salon-krasoty':
                case 'medicina':
                case 'yuridicheskie-uslugi':
                case 'nedvizhimost':
                case 'banki-strahovanie':
                case 'site-promition':
                case 'cafe-new':
                case 'tech-support':
			    case 'pro-text':
				case 'medijnaya-reklama':
                case 'vibor':
                case 'sponsorship':
                case 'partner-programm':
                case 'market':
                case 'action-webcom-vk':
                case 'company':
                case 'seo-cost':
                case 'moneyback':
                case 'audit':
                case 'marketing-audit':
                case 'technical-audit':
                case 'complex-audit':
                case 'kr-audit':
                case 'seo-audit':
                case 'audit-new':
                case 'kr-google':
                case 'kr-yandex':
                case 'kr-cost':
				case 'search-optimtzation-new':
                case 'kp-new':
                case 'videoreklama-new':
			    case 'target-advert-new':
				case 'paketnye-new':
                case 'sy-new':
				case 'youtube-new':
				case 'instagram-new':
                case 'context-new-advert':
                case 'remarketing-new':
                case 'web-analytics':
                case 'ok-new':
                case 'vk-new':
                case 'mobile-new':
                case 'e-commerce-new':
                case 'kr-search':
                case 'y-market-new':
                case 'seo-new':
                case 'consalting':
                case 'advert-mobile-new':
                case 'test-drive-new':
                case 'facebook':
                case 'contacts':
                case 'online-request':
                case 'online-call':
                    $('.b-form__loader').hide();
                    $('.b-form__information').hide();
                    break;
                //Online-request
                //Online-call
                case 'online-request':
                    $('[data-form-id="online-request"] .b-form__information').hide();
                    $('[data-form-id="online-request"] .b-form__loader').hide();
                    break;
                case 'online-call':
                    $('[data-form-id="online-call"] .b-form__information').hide();
                    $('[data-form-id="online-call"] .b-form__loader').hide();
                    break;
        }
    }

    /* Put test data in fields */
    (function ()
    {
        window.putTD = function ()
        {
            $('input[name=name]').val('tp_test');
            $('input[name=phone]').val('123123123');
            $('input[name=email]').val('test@te.st');
            $('input[name=site]').val('te.st');
            $('input[name=time]').val('18:00');
            $('textarea[name=text]').val('Test');
        }
    }());
});
