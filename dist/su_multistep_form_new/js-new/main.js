$(document).ready(function() {

    var formSteps = {
        "option-1" : {
            "step-1" :  {
                "step-index": 1,
                "data-step": 0
            },
            "step-2" : {
                "step-index": 2,
                "data-step": 1
            },
            "step-3" : {
                "step-index": 3,
                "data-step": 6
            },
            "step-4" :  {
                "step-index": 4,
                "data-step": 8
            },
            "length" : 4
        }, /* непубличный договор */
        "option-2" : {
            "step-1" : {
                    "step-index": 1,
                    "data-step": 0
                },
            "step-2" : {
                "step-index": 2,
                "data-step": 1
            },
            "step-3" : {
                "step-index": 3,
                "data-step": 2
            },
            "step-4" : {
                "step-index": 4,
                "data-step": 3
            },
            "step-5" : {
                "step-index": 5,
                "data-step": 4
            },
            "step-6" : {
                "step-index": 6,
                "data-step": 5
            },
            "step-7" : {
                "step-index": 7,
                "data-step": 8
            },
            "length" : 7
        }, /* публичный договор юрлицо */
        "option-3" : {
            "step-1" : {
                "step-index": 1,
                "data-step": 0
            },
            "step-2" : {
                "step-index": 2,
                "data-step": 1
            },
            "step-3" : {
                "step-index": 3,
                "data-step": 2
            },
            "step-4" : {
                "step-index": 4,
                "data-step": 3
            },
            "step-5" : {
                "step-index": 5,
                "data-step": 7
            },
            "length" : 5
        } /* публичный договор ип */
    },
    options = formSteps["option-1"],
    step = options["step-1"]["step-index"];

    console.log(formSteps);
    $(".b-form-steps-list").owlCarousel({
        items:1,
        autoHeight:true,
        mouseDrag:false,
        touchDrag:false,
        pullDrag:false,
        animateIn:"fadeIn"
    });

    $("body").on("submit", ".b-form-steps form", function () {
       return false;
    });

    $("body").on("click", ".js-next-step", function () {
        validateForm($(this).closest(".active"));
        var parentIrem = $(this).closest(".b-form-steps-item");
        if(parentIrem.find(".invalid").length > 0) return false;
        if(parentIrem.attr("data-step") === "1" || parentIrem.attr("data-step") === "2") {
            options = formSteps["option-" + parentIrem.find("input:checked").attr("data-option") + ""];
        }
        step = step + 1;
        $(this).closest(".b-form-steps-list").trigger('to.owl.carousel', options["step-"+ step +""]["data-step"]);
        return false;
    });

    $("body").on("click", ".js-prev-step", function () {
        step = step - 1;
        $(this).closest(".b-form-steps-list").trigger('to.owl.carousel', options["step-"+ step +""]["data-step"]);
        return false;
    });

    $("body").on("click", ".js-send-form", function () {
        $(this).closest(".b-form-steps-list").trigger('to.owl.carousel', 8);
        return false;
    });

    $("body").on("input change", ".active *[data-required]", function () {
        validateForm($(this).closest(".input-wrap"));
        return false;
    });

    $("body").on("change", ".fields-group input[type='checkbox']", function () {
        if($(this).prop("checked")) {
            $(this).siblings(".input-wrap").slideDown();
        } else {
            $(this).siblings(".input-wrap").slideUp();
        }
    });

    $("body").on("change", ".b-step-card-agreement input", function () {
        var button = $(this).closest(".b-step-card-footer").find(".b-btn");
        if($(this).prop("checked")) {
            button.removeClass("disabled");
        } else {
            button.addClass("disabled");
        }
    });

    $("body").on("click", ".js-agreement-link", function () {
        $(this).closest(".b-step-card-agreement").find(".b-form__dropdown").slideToggle();
        return false;
    });

    $("body").on("click", ".b-step-card-agreement .js-close-btn", function () {
        $(this).closest(".b-form__dropdown").slideUp();
        return false;
    });

    $("body").on("change", ".fields-item select", function () {
        if($(this).val() === "1") {
            $(this).closest(".fields-item").siblings(".attach-field").removeClass("invalid success").find("input").addClass("disabled").val('');
        } else {
            $(this).closest(".fields-item").siblings(".attach-field").find("input").removeClass("disabled");
        }
        return false;
    });

    $("body").on("change", ".checking-account input[type='checkbox']", function () {
        var block = $(this).closest(".b-form-steps-fields");
        if($(this).prop("checked")) {
            block.find(".checking-account .input-wrap input").removeClass("disabled").removeAttr("tabindex");
        } else {
            block.find(".checking-account").removeClass("invalid success").find(".input-wrap input").addClass("disabled").attr("tabindex", "-1").val('');
        }
    });
})

function validateForm(form) {
    var valide = true;
    form.find("*[data-required]:not('.disabled'), *[data-hidden]:not('.disabled')").each(function () {
        switch ($(this).attr("data-name")) {
            case "name":
                valide = nameValidate($(this))
                break;
            case "length":
                valide = lengthValidate($(this))
                break;
            case "phone":
                valide = phoneValidate($(this))
                break;
            case "email":
                valide = emailValidate($(this))
                break;
            case "site":
                valide = siteValidate($(this))
                break;
            case "checking-account":
                valide = checkAccValidate($(this))
                break;
            case "summ":
                valide = summValidate($(this))
                break;
        }
        if(!valide) {
            $(this).closest(".fields-item").removeClass("success").addClass("invalid");
        } else if(valide && valide !== "break") {
            $(this).closest(".fields-item").removeClass("invalid").addClass("success");
        }
    });
    return valide;
}

function nameValidate(field) {
    if(field.val().length < 6) {
        return false;
    }
    return true;
}

function lengthValidate(field) {
    if(field.val().length !== +field.attr("data-length")) {
        return false;
    }
    return true;
}

function phoneValidate(field) {
    var patternBel = new RegExp(/^(\+?(375|8)[\s]?)(\(?(17|162|163|1644|165|1655|212|2151|2156|2157|214|232|2334|2351|2342|152|1512|1561|1592|1514|177|1771|1772|1775|1773|1795|174|1717|222|2232|225|2231|2233|2248|2237|2244|2236|2245|2238|2241|2234|2240|2235|2246|2247|2242|2243|2239|33|44|25|29|0(17|162|163|1644|165|1655|212|2151|2156|2157|214|232|2334|2351|2342|152|1512|1561|1592|1514|177|1771|1772|1775|1773|1795|174|1717|222|2232|225|2231|2233|2248|2237|2244|2236|2245|2238|2241|2234|2240|2235|2246|2247|2242|2243|2239|25|29|33|44))\)?[\s]?)(\d{2,3}[-\s]?)(\d{2}[-\s]?){2}$/i),
        patternRus = new RegExp(/^(\+?(7)[\s]?)(\(?\d{3}\)?[\s]?)(\d{3}[-\s]?)(\d{2}[-\s]?){2}$/i),
        patternOther = new RegExp(/^(\+\d)/i),
        phone = field.val();

    if (!phone.indexOf("+375") || !phone.indexOf("375") || !phone.indexOf("80")) {
        return patternBel.test(phone);
    }
    else if (!phone.indexOf("+7") || !phone.indexOf("7")) {
        return patternRus.test(phone);
    }
    else if (!phone.indexOf("+")) {
        return patternOther.test(phone);
    }
}

function emailValidate(field) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i),
        emailAddress = field.val();
    return pattern.test(emailAddress);
}

function siteValidate(field) {
    var pattern = new RegExp(/^(([а-яёa-z0-9]{1})((\.[а-яёa-z0-9-])|([а-яёa-z0-9-]))*\.([а-яёa-z]{2,7})(\/?)$)/i),
        siteAddress = field.val();
    return pattern.test(siteAddress);
}

function summValidate(field) {
    if(field.closest(".input-wrap").siblings("input[type='checkbox']").prop("checked")) {
        var val = isNaN(parseInt(field.val())) ? 0 : parseInt(field.val());
        if(val < 100) {
            return false;
        }
        return true;
    }
    return "break";
}

function checkAccValidate(field) {
    var pattern = new RegExp(/^[A-Z]{2}\d{2}[A-Z]{4}\d{20}$/),
        checkAcc = field.val();
    return pattern.test(checkAcc);
}
