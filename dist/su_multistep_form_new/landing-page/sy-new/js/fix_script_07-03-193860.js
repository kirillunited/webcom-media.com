jQuery(document).ready(function ($) {
	// Временный bug fix для страницы pay.webcom-group.by:
	// заменить домен только в относительных ссылках на основной домен promo-webcom.by
	var hostName = window.location.hostname;
	if (hostName == "www.pay.webcom-group.by") {
		var link = $('a') || 0;
		
		link.each(function () {
			var mainDomain = "www.promo-webcom.by",
				linkHref = $(this).attr('href') || '',
				linkHrefModified = '//' + mainDomain + linkHref;

			if (	
					linkHref.substring(1, -1) !== '' &&
					linkHref.substring(1, -1) !== '#' &&
					linkHref.substring(2, -1) !== '//' &&
					linkHref.substring(3, -1) !== 'tel' &&
					linkHref.substring(3, -1) !== 'www' &&
					linkHref.substring(4, -1) !== 'http' &&
					linkHref.substring(6, -1) !== 'mailto' &&
					linkHref.substring(10, -1) !== 'javascript'
				)
			{
				$(this).attr('href', linkHrefModified);
				// console.log('original link: ' + linkHref + '  modified link: ' + linkHrefModified);

			} else if ( linkHref.indexOf(hostName) != -1 ) {
				var linkHrefModified = linkHref.replace(hostName, mainDomain);
				$(this).attr('href', linkHrefModified);
				// console.log('original link: ' + linkHref + '  modified link: ' + linkHrefModified);
			}
		});
	}
});